package dev.havencare.homepage.mapper.vo;

import lombok.Data;
import java.sql.Date;
import java.sql.Time;

@Data
public class BoardVo {

  public String boardCode;

  public String boardType;

  public String boardTitle;

  public String boardText;

  public String viewStartDate;

  public String viewEndDate;

  public int viewCount;

  public String insertDt;

  public String updateDt;

  public String deleteDt;
}
