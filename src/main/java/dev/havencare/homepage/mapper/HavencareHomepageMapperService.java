package dev.havencare.homepage.mapper;

import dev.havencare.homepage.mapper.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class HavencareHomepageMapperService {

  @Autowired
   HavencareHomepageMapper mapper;

  public void insertBoard(String boardCode, String boardType, String boardTitle, String boardText, String viewStartDate, String viewEndDate) {
    mapper.insertBoard(boardCode, boardType, boardTitle, boardText, viewStartDate, viewEndDate);
  }

  public void updateBoard(String boardCode, String boardType, String boardTitle, String boardText, String viewStartDate, String viewEndDate) {
    mapper.updateBoard(boardCode, boardType, boardTitle, boardText, viewStartDate, viewEndDate);
  }

  public void deleteBoard(String boardCode) {
    mapper.deleteBoard(boardCode);
  }

  public BoardVo selectBoard(String boardCode) {
    return mapper.selectBoard(boardCode);
  }

  public List<BoardVo> selectListBoard(String boardType, String searchWord) {
    return mapper.selectListBoard(boardType, searchWord);
  }
}
