package dev.havencare.homepage.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import dev.havencare.homepage.mapper.vo.*;
import java.util.List;
@Mapper
public interface HavencareHomepageMapper {

  void insertBoard(@Param("boardCode") String boardCode, @Param("boardType") String boardType, @Param("boardTitle") String boardTitle, @Param("boardText") String boardText, @Param("viewStartDate") String viewStartDate, @Param("viewEndDate") String viewEndDate);

  void updateBoard(@Param("boardCode") String boardCode, @Param("boardType") String boardType, @Param("boardTitle") String boardTitle, @Param("boardText") String boardText, @Param("viewStartDate") String viewStartDate, @Param("viewEndDate") String viewEndDate);

  void deleteBoard(@Param("boardCode") String boardCode);

  BoardVo selectBoard(@Param("boardCode") String boardCode);

  List<BoardVo> selectListBoard(@Param("boardType") String boardType, @Param("searchWord") String searchWord);
}
