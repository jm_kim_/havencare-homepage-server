package dev.havencare.homepage.controller.response;

import dev.havencare.homepage.mapper.vo.BoardVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import dev.havencare.homepage.controller.vo.Board;
import java.util.HashMap;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "게시판 조회")
@Data
public class GetBoardResponse extends BaseResponse {

  public BoardVo board = null;
}
