package dev.havencare.homepage.controller.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import dev.havencare.homepage.controller.vo.Board;
import java.util.HashMap;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "게시판 등록")
@Data
public class AddBoardResponse extends BaseResponse {

  public Board board = null;
}
