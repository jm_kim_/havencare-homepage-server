package dev.havencare.homepage.controller.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import dev.havencare.homepage.controller.vo.Board;
import java.util.HashMap;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "게시판 수정")
@Data
public class ReviseBoardResponse extends BaseResponse {

  public Board board = null;
}
