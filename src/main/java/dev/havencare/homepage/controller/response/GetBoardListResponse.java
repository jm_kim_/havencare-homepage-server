package dev.havencare.homepage.controller.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import dev.havencare.homepage.controller.vo.Board;
import java.util.HashMap;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@ApiModel(description = "게시판 목록 조회")
@Data
public class GetBoardListResponse extends BaseResponse {

  public List<Board> boards = null; // 게시판 목록
}
