package dev.havencare.homepage.controller.handler;

import dev.havencare.homepage.controller.request.*;
import dev.havencare.homepage.controller.response.*;
import dev.havencare.homepage.controller.util.Converter;
import dev.havencare.homepage.controller.vo.Board;
import dev.havencare.homepage.mapper.HavencareHomepageMapper;
import dev.havencare.homepage.mapper.vo.BoardVo;
import dev.havencare.homepage.service.CustomUserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import dev.havencare.homepage.controller.ResultCode;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
public class GetBoardListHandler extends BaseHandler {

    @Autowired
    private HavencareHomepageMapper mapper;

    public GetBoardListResponse execute(CustomUserDetails user, GetBoardListRequest req) {
        GetBoardListResponse res = new GetBoardListResponse();

        final String boardType = req.getBoardType();
        final String searchWord = req.getSearchWord();
        final String viewStartDate = req.getViewStartDate();
        final String viewEndDate = req.getViewEndDate();

        if (emptyParam(boardType)) {
            res.setCode(ResultCode.BadParams);
            return res;
        }

        try {

            List<BoardVo> list = this.mapper.selectListBoard(boardType, searchWord);
            List<Board> boards = new ArrayList<>();
            boards = Converter.convert(list, boards.getClass());
            res.boards = boards;

            // code here
            res.setCode(ResultCode.Success);
            return res;
        } catch (Exception e) {
            log.error(e.toString());
            res.setCode(ResultCode.Failed);
            return res;
        }
    }
}
