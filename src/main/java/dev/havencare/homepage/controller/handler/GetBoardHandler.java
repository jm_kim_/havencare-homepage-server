package dev.havencare.homepage.controller.handler;

import dev.havencare.homepage.controller.request.*;
import dev.havencare.homepage.controller.response.*;
import dev.havencare.homepage.mapper.HavencareHomepageMapper;
import dev.havencare.homepage.mapper.vo.BoardVo;
import dev.havencare.homepage.service.CustomUserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import dev.havencare.homepage.controller.ResultCode;

@Slf4j
@Component
public class GetBoardHandler extends BaseHandler {

  @Autowired
  private HavencareHomepageMapper mapper;

  public GetBoardResponse execute(CustomUserDetails user, GetBoardRequest req) {
    GetBoardResponse res = new GetBoardResponse();

    final String boardCode = req.getBoardCode();

    if(emptyParam(boardCode)) {
      res.setCode(ResultCode.BadParams);
      return res;
    }

    try {
      // code here
      res.setCode(ResultCode.Success);
      res.board = this.mapper.selectBoard(boardCode);

      return res;
    }
    catch(Exception e) {
      log.error(e.toString());
      res.setCode(ResultCode.Failed);
      return res;
    }
  }
}
