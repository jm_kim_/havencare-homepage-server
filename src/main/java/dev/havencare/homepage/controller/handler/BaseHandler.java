package dev.havencare.homepage.controller.handler;


public class BaseHandler {

    protected boolean emptyParam(String value) {
        if(value == null || value.length() < 1 || value.trim().length() < 1) return true;
        return false;
    }

}
