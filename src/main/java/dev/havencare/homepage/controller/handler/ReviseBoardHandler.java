package dev.havencare.homepage.controller.handler;

import dev.havencare.homepage.controller.request.*;
import dev.havencare.homepage.controller.response.*;
import dev.havencare.homepage.controller.util.BaseConstants;
import dev.havencare.homepage.controller.util.CodeGenerator;
import dev.havencare.homepage.controller.util.Converter;
import dev.havencare.homepage.controller.vo.Board;
import dev.havencare.homepage.mapper.HavencareHomepageMapper;
import dev.havencare.homepage.mapper.vo.BoardVo;
import dev.havencare.homepage.service.CustomUserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import dev.havencare.homepage.controller.ResultCode;

@Slf4j
@Component
public class ReviseBoardHandler extends BaseHandler {

  @Autowired
  private HavencareHomepageMapper mapper;

  public ReviseBoardResponse execute(CustomUserDetails user, ReviseBoardRequest req) {
    ReviseBoardResponse res = new ReviseBoardResponse();

    final String boardCode = req.getBoardCode();
    final String boardType = req.getBoardType();
    final String boardTitle = req.getBoardTitle();
    final String boardText = req.getBoardText();
    final String viewStartDate = req.getViewStartDate();
    final String viewEndDate = req.getViewEndDate();

    if(emptyParam(boardCode)||emptyParam(boardType)||emptyParam(boardTitle)||emptyParam(boardText)) {
      res.setCode(ResultCode.BadParams);
      return res;
    }

    try {
      // code here
      this.mapper.updateBoard(boardCode, boardType, boardTitle, boardText, viewStartDate, viewEndDate);

      BoardVo vo = this.mapper.selectBoard(boardCode);
      Board board = Converter.convert(vo, Board.class);

      res.board = board;

      res.setCode(ResultCode.Success);
      return res;
    }
    catch(Exception e) {
      log.error(e.toString());
      res.setCode(ResultCode.Failed);
      return res;
    }
  }
}
