package dev.havencare.homepage.controller.handler;

import dev.havencare.homepage.controller.request.*;
import dev.havencare.homepage.controller.response.*;
import dev.havencare.homepage.controller.util.Converter;
import dev.havencare.homepage.controller.vo.Board;
import dev.havencare.homepage.mapper.HavencareHomepageMapper;
import dev.havencare.homepage.mapper.vo.BoardVo;
import dev.havencare.homepage.service.CustomUserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import dev.havencare.homepage.controller.ResultCode;

@Slf4j
@Component
public class RemoveBoardHandler extends BaseHandler {

  @Autowired
  private HavencareHomepageMapper mapper;

  public RemoveBoardResponse execute(CustomUserDetails user, RemoveBoardRequest req) {
    RemoveBoardResponse res = new RemoveBoardResponse();

    final String boardCode = req.getBoardCode();

    if(emptyParam(boardCode)) {
      res.setCode(ResultCode.BadParams);
      return res;
    }

    try {
      // code here
      this.mapper.deleteBoard(boardCode);
      res.setCode(ResultCode.Success);
      return res;
    }
    catch(Exception e) {
      log.error(e.toString());
      res.setCode(ResultCode.Failed);
      return res;
    }
  }
}
