/****************************************************************************************************************
 * ■ 시 스 템  명 :  potSystem
 * ■ 프로그램  명 : java.lang.String support Class
 * ■ 프로그램개요 :
 * ■ 처리로직개요
 *  1. 없음
 * ■ 관련테이블 : 없음
 * ※
 * -------------------------------------------------------------------------------------------------------------
 * ■ 이 력
 * -------------------------------------------------------------------------------------------------------------
 *           작성일자                      버전             작성자               내용
 * -------------------------------------------------------------------------------------------------------------
 *          2007. 09. 11       1.0      이종영
 * -------------------------------------------------------------------------------------------------------------
 * Copyright(c) 2010 BlueRiver All Page content is property of BlueRiver
 ****************************************************************************************************************/
package dev.havencare.homepage.controller.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

	private static Pattern pattern;
	private static Matcher matcher;
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public static final String DELIMIT = ";";

	public static List<String> getStringToken(String str, String delimit) {
		try {
			if(delimit == null) {
				delimit = DELIMIT;
			}
			StringTokenizer token = new StringTokenizer(str, delimit);
			ArrayList<String> list = new ArrayList<String>();
			while(token.hasMoreTokens()) {
				list.add(token.nextToken(delimit).trim());
			}
			return list;
		} catch(Exception e) {

		}
		return null;
	}

	public static boolean isEmpty(String string) {
		if(null == string) {
			return true;
		}

		if ("".equals(string)) {
			return true;
		}

		return false;
	}

	public static List<String> getStringToken(String str) throws Exception {
		return getStringToken(str, null);
	}

	public static String getString(String string, String sourceEncoding, String targetString) {
		try {
			return new String(string.getBytes(sourceEncoding), targetString);
		} catch(Exception e) {

		}

		return null;
	}

	public static String changeUTF8(String string, String sourceEncoding) {
		return getString(string, sourceEncoding, "UTF-8");
	}

	public static String changeEUCKR(String string, String sourceEncoding) {
		return getString(string, sourceEncoding, "EUC-KR");
	}

	public static String changeMS949(String string, String sourceEncoding) {
		return getString(string, sourceEncoding, "MS949");
	}

	public static String change8859(String string, String sourceEncoding) {
		return getString(string, sourceEncoding, "ISO-8859-1");
	}

	public static String removeHTML(String resource) {
		String regex = "<(/)?([a-zA-Z]*)(\\s[a-zA-Z]*=[^>]*)?(\\s)*(/)?>";
		return resource.replaceAll(regex, "");
	}


	public static String convertEucToUTF8(String source) {
		try {
			return new String(source.getBytes("EUC-kr"), "UTF-8");
		} catch(UnsupportedEncodingException e) {
		}
		return null;

	}

	/**
	 * Validate hex with regular expression
	 *
	 * @param hex
	 *            hex for validation
	 * @return true valid hex, false invalid hex
	 */
	public static boolean validateEmail(final String hex) {
		if(null == pattern) {
			pattern = Pattern.compile(EMAIL_PATTERN);
		}
		matcher = pattern.matcher(hex);
		return matcher.matches();
	}

	public static void main(String[] args) {
		String source = "Tiles 설정파일입니다.";
		String a = StringUtils.convertEucToUTF8(source);

		System.out.println(a);
	}


	public static String getString(InputStream is) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();
	}

	public static String getString(InputStream is, String encoding) {

		BufferedReader br = null;
		StringBuilder sb = new StringBuilder();

		String line;
		try {

			br = new BufferedReader(new InputStreamReader(is, encoding));
			while ((line = br.readLine()) != null) {
				sb.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return sb.toString();
	}

	public static void findStringCharset(String word) {

		String [] charSet = {"utf-8","euc-kr","ksc5601","iso-8859-1","x-windows-949"};

		for (int i=0; i<charSet.length; i++) {
			for (int j=0; j<charSet.length; j++) {
				try {
					System.out.println("[" + charSet[i] +"," + charSet[j] +"] = " + new String(word.getBytes(charSet[i]), charSet[j]));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static class CharEncoding {

		public static String encode(String str, String charset) {
			StringBuilder sb = new StringBuilder();
			try {
				byte[] key_source = str.getBytes(charset);
				for(byte b : key_source) {
					String hex = String.format("%02x", b).toUpperCase();
					sb.append("%");
					sb.append(hex);
				}
			} catch(UnsupportedEncodingException e) { }//Exception
			return sb.toString();
		}

		public static String decode(String hex, String charset) {
			byte[] bytes = new byte[hex.length()/3];
			int len = hex.length();
			for(int i = 0 ; i < len ;) {
				int pos = hex.substring(i).indexOf("%");
				if(pos == 0) {
					String hex_code = hex.substring(i+1, i+3);
					bytes[i/3] = (byte)Integer.parseInt(hex_code, 16);
					i += 3;
				} else {
					i += pos;
				}
			}
			try {
				return new String(bytes, charset);
			} catch(UnsupportedEncodingException e) { }//Exception
			return "";
		}

		public static String changeCharset(String str, String charset) {
			try {
				byte[] bytes = str.getBytes(charset);
				return new String(bytes, charset);
			} catch(UnsupportedEncodingException e) { }//Exception
			return "";
		}
	}

}
