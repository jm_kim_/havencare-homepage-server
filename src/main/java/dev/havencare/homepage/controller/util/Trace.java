package dev.havencare.homepage.controller.util;

/**
 * Created by ariman on 2016-02-24.
 */
public class Trace {

    public static void log(Throwable throwable) { throwable.printStackTrace(); }
    public static void log(Exception e) {
        e.printStackTrace();
    }
    public static void log(String e) { System.out.println(e); }
}
