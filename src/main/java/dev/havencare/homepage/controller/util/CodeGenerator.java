package dev.havencare.homepage.controller.util;

import java.util.Calendar;

/**
 * Created by thecloud on 2015-10-14.
 */
public class CodeGenerator {

    public static String getCode(String prefix, String delimeter) {
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            Trace.log(e);
        }

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        StringBuffer buffer = new StringBuffer();

        if(null != prefix && prefix.length() > 0) {
            buffer.append(prefix);
        }

        buffer.append(DateUtil.getDateUntilDay(calendar, ""));

        if(null != delimeter && delimeter.length() > 0) {
            buffer.append(delimeter);
        }

        if(calendar.get(Calendar.HOUR_OF_DAY) < 10) {
            buffer.append("0");
        }
        buffer.append(calendar.get(Calendar.HOUR_OF_DAY));
        if(calendar.get(Calendar.MINUTE) < 10) {
            buffer.append("0");
        }
        buffer.append(calendar.get(Calendar.MINUTE));
        if(calendar.get(Calendar.SECOND) < 10) {
            buffer.append("0");
        }
        buffer.append(calendar.get(Calendar.SECOND));
        if (calendar.get(Calendar.MILLISECOND) < 100) {
            buffer.append("0");
        }
        if(calendar.get(Calendar.MILLISECOND) < 10) {
            buffer.append("0");
        }
        buffer.append(calendar.get(Calendar.MILLISECOND));

        return buffer.toString();
    }

    public static String getCode(String prefix) {
        return getCode(prefix, "");
    }

    public static void main(String[] args) {

//        for(int i = 0; i < 1; i++) {
            System.out.println(CodeGenerator.getCode("PATI", ""));
//            try {
//                Thread.sleep(200);
//            } catch (InterruptedException e) {
//                Trace.log(e);
//            }
//        }
    }
}
