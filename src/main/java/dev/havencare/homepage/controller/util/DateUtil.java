package dev.havencare.homepage.controller.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DateUtil {
	
	public static final int YEAR = 1;
	public static final int MONTH = 2;
	public static final int DAY = 3;
	public static final int HOUR = 4;
	public static final int MINUTE = 5;
	public static final int SECOND = 6;

	public static final int DAY_OF_SECOND = 60 * 60 * 24;
	public static final int DAY_OF_MINUTE = 60 * 24;


    /**
     * 유닉스타임과 현재시간 비교 (초단위까지)
     * @param unixTime
     * @param compare
     * @return
     */
	public static boolean isEquals(String unixTime, int compare) {
		return isEquals(Long.parseLong(unixTime), compare);
	}

    /**
     *
     * 유닉스타임과 현재시간 비교 (초단위까지)
     * @param unixTime
     * @param compare
     * @return
     */
	public static boolean isEquals(long unixTime, int compare) {
		try {
			if(YEAR == compare) {
				return isEqualsYear(unixTime);
			} else if(MONTH == compare) {
				return isEqualsMonth(unixTime);
			} else if(DAY == compare) {
				return isEqualsDay(unixTime);
			} else if(HOUR == compare) {
				return isEqualsHour(unixTime);
			} else if(MINUTE == compare) {
				return isEqualsMinute(unixTime);
			} else if(SECOND == compare) {
				return isEqualsSecond(unixTime);
			}
		} catch(Exception e) { }
		return false;
	}

    /**
     * 유닉스타임을 현재시간으로 변환
     * @param unixTime
     * @param compare
     * @return
     */
	public static String getDateFromUnixTime(String unixTime, int compare) {
		try {
			return getDateFromUnixTime(Long.parseLong(unixTime), compare);
		} catch(Exception e) { }
		return "1970.01.01 00:00:00";
	}

    /**
     *
     * 유닉스타임을 현재시간으로 변환
     * @param unitTime
     * @param compare
     * @return
     */
	public static String getDateFromUnixTime(long unitTime, int compare) {
        return getDateFromUnixTime(unitTime, compare, ".");
    }

    public static String getDateFromUnixTime(long unitTime, int compare, String delimiter) {
		Calendar calendar =  Calendar.getInstance();
		calendar.setTimeInMillis(unitTime);

        if(null == delimiter) {
            delimiter = "";
        }
		
		StringBuffer buffer = new StringBuffer();
		buffer.append(calendar.get(Calendar.YEAR));
		
		if(compare > YEAR) {
			buffer.append(delimiter);
			if(calendar.get(Calendar.MONTH) + 1 < 10) {
				buffer.append("0");
			}
			buffer.append((calendar.get(Calendar.MONTH) + 1));
		}
		
		if(compare > MONTH) {
			buffer.append(delimiter);
			if(calendar.get(Calendar.DAY_OF_MONTH) < 10) {
				buffer.append("0");
			}
			buffer.append(calendar.get(Calendar.DAY_OF_MONTH));
		}
		
		if(compare > DAY) {
            if(!"".equals(delimiter)) {
                buffer.append(" ");
            }
			buffer.append(delimiter);
			if(calendar.get(Calendar.HOUR_OF_DAY) < 10) {
				buffer.append("0");
			}
			buffer.append(calendar.get(Calendar.HOUR_OF_DAY));
		}
		
		if(compare > HOUR) {
            if(!"".equals(delimiter)) {
                buffer.append(":");
            }
			if(calendar.get(Calendar.MINUTE) < 10) {
				buffer.append("0");
			}
			buffer.append(calendar.get(Calendar.MINUTE));
		}
		
		if(compare > MINUTE) {
            if(!"".equals(delimiter)) {
                buffer.append(":");
            }
			if(calendar.get(Calendar.SECOND) < 10) {
				buffer.append("0");
			}
            buffer.append(calendar.get(Calendar.SECOND));
		}

		return buffer.toString();
	}
	
	private static boolean isEqualsYear(long unixTime) throws Exception {
		int now = Integer.parseInt(getDateUntilYear(null));
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(unixTime);
		int day = Integer.parseInt(getDateUntilYear(cal));
		if(day == now) {
			return true;
		}
		return false;
	}
	public static boolean isEqualsMonth(long unixTime) throws Exception {
		int now = Integer.parseInt(getDateUntilMonth(null, ""));
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(unixTime);
		int day = Integer.parseInt(getDateUntilMonth(cal, ""));
		if(day == now) {
			return true;
		}
		return false;
	}
	public static boolean isEqualsDay(long unixTime) throws Exception {
		int now = Integer.parseInt(getDateUntilDay(null, ""));
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(unixTime);
		int day = Integer.parseInt(getDateUntilDay(cal, ""));
		if(day == now) {
			return true;
		}
		return false;
	}
	public static boolean isEqualsHour(long unixTime) throws Exception {
		long now = Long.parseLong(getDateUntilHour(null, ""));
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(unixTime);
		long day = Long.parseLong(getDateUntilHour(cal, ""));
		if(day == now) {
			return true;
		}
		return false;
	}
	public static boolean isEqualsMinute(long unixTime) throws Exception {
		long now = Long.parseLong(getDateUntilMinute(null, ""));
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(unixTime);
		long day = Long.parseLong(getDateUntilMinute(cal, ""));
		
		if(day == now) {
			return true;
		}
		return false;
	}
	
	public static boolean isEqualsSecond(long unixTime) throws Exception {
		long now = Long.parseLong(getDateUntilSecond(null, ""));
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(unixTime);
		long day = Long.parseLong(getDateUntilSecond(cal, ""));
		if(day == now) {
			return true;
		}
		return false;
	}
	
	public static String getDateUntilSecond(Calendar calendar, String delimiter) {
        if(null == delimiter) {
            delimiter = "";
        }

		StringBuffer buffer = new StringBuffer();
        buffer.append(getDateUntilMinute(calendar, delimiter));

        if(!"".equals(delimiter)) {
            buffer.append(":");
        }

		if(calendar.get(Calendar.SECOND) < 10) {
			buffer.append("0");
		}
		buffer.append(calendar.get(Calendar.SECOND));
		return buffer.toString();
	}
	
	public static String getDateUntilMinute(Calendar calendar, String delimiter) {
        if(null == delimiter) {
            delimiter = "";
        }

		StringBuffer buffer = new StringBuffer();
        buffer.append(getDateUntilHour(calendar, delimiter));
        buffer.append(":");

		if(calendar.get(Calendar.MINUTE) < 10) {
			buffer.append("0");
		}
		buffer.append(calendar.get(Calendar.MINUTE));
		return buffer.toString();
	}
	
	public static String getDateUntilHour(Calendar calendar, String delimiter) {
        if(null == delimiter) {
            delimiter = "";
        }

		StringBuffer buffer = new StringBuffer();
        buffer.append(getDateUntilDay(calendar, delimiter));
        buffer.append(delimiter);

		if(calendar.get(Calendar.HOUR_OF_DAY) < 10) {
			buffer.append("0");
		}

        if(!"".equals(delimiter)) {
            buffer.append(" ");
        }
		buffer.append(calendar.get(Calendar.HOUR_OF_DAY));
		return buffer.toString();
	}

	public static String getDateUntilTimes(Calendar calendar, String delimiter) {
		if(null == delimiter) {
			delimiter = "";
		}
		StringBuffer buffer = new StringBuffer();
		if(calendar.get(Calendar.HOUR_OF_DAY) < 10) {
			buffer.append("0");
		}

		buffer.append(calendar.get(Calendar.HOUR_OF_DAY));
		buffer.append(delimiter);

		if(calendar.get(Calendar.MINUTE) < 10) {
			buffer.append("0");
		}
		buffer.append(calendar.get(Calendar.MINUTE));
		buffer.append(delimiter);

		if(calendar.get(Calendar.SECOND) < 10) {
			buffer.append("0");
		}
		buffer.append(calendar.get(Calendar.SECOND));
		return buffer.toString();
	}
	
	public static String getDateUntilDay(Calendar calendar, String delimiter) {
        if(null == delimiter) {
            delimiter = "";
        }
		StringBuffer buffer = new StringBuffer();
        buffer.append(getDateUntilMonth(calendar, delimiter));
        buffer.append(delimiter);

		if(calendar.get(Calendar.DAY_OF_MONTH) < 10) {
			buffer.append("0");
		}
		buffer.append(calendar.get(Calendar.DAY_OF_MONTH));

		return buffer.toString();
	}
	
	public static String getDateUntilMonth(Calendar calendar, String delimiter) {
        if(null == delimiter) {
            delimiter = "";
        }

		StringBuffer buffer = new StringBuffer();
		buffer.append(getDateUntilYear(calendar));
        buffer.append(delimiter);

		if(calendar.get(Calendar.MONTH) + 1 < 10) {
			buffer.append("0");
		}
		buffer.append((calendar.get(Calendar.MONTH) + 1));
		return buffer.toString();
	}
	
	public static String getDateUntilYear(Calendar calendar) {
		if(null == calendar) {
			calendar = Calendar.getInstance();
		}
		StringBuffer buffer = new StringBuffer();
		buffer.append(calendar.get(Calendar.YEAR));
		return buffer.toString();
	}

	public static Date getDateFromStringFormat(String date, String format) {
		SimpleDateFormat transFormat = new SimpleDateFormat(format);
		try {
			Date to = transFormat.parse(date);
			return to;
		} catch (ParseException e) {
			Trace.log(e);
		}
		return null;
	}

	public static String getStringDateFromDate(Date date, String format) {
		SimpleDateFormat transFormat = new SimpleDateFormat(format);
		String to = transFormat.format(date);
		return to;
	}

	public static List<String> getWeekdayList(String compareDate, String delimiter) throws Exception {

		Calendar cal = Calendar.getInstance();
		int toYear = 0;
		int toMonth = 0;
		int toDay = 0;
		if(compareDate == null || compareDate.equals("")) {   //파라메타값이 없을경우 오늘날짜
			toYear = cal.get(cal.YEAR);
			toMonth = cal.get(cal.MONTH)+1;
			toDay = cal.get(cal.DAY_OF_MONTH);
			int yoil = cal.get(cal.DAY_OF_WEEK); //요일나오게하기(숫자로)

			if(yoil != 1) {   //해당요일이 일요일이 아닌경우
				yoil = yoil-2;
			} else {           //해당요일이 일요일인경우
				yoil = 7;
			}
			cal.set(toYear, toMonth-1, toDay-yoil);  //해당주월요일로 세팅
		} else {
			List<String> dates = StringUtils.getStringToken(compareDate, delimiter);
//			int yy =Integer.parseInt(yyyymmdd.substring(0, 4));
//			int mm =Integer.parseInt(yyyymmdd.substring(4, 6))-1;
//			int dd =Integer.parseInt(yyyymmdd.substring(6, 8));

			int yy = Integer.parseInt(dates.get(0));
			int mm = Integer.parseInt(dates.get(1)) - 1;
			int dd = Integer.parseInt(dates.get(2));

			cal.set(yy, mm, dd);
		}

		List<String> arrYMD = new ArrayList<>();

		int inYear = cal.get(cal.YEAR);
		int inMonth = cal.get(cal.MONTH);
		int inDay = cal.get(cal.DAY_OF_MONTH);
		int yoil = cal.get(cal.DAY_OF_WEEK); //요일나오게하기(숫자로)
		if(yoil != 1) {   //해당요일이 일요일이 아닌경우
			yoil = yoil-2;
		} else {           //해당요일이 일요일인경우
			yoil = 7;
		}
		inDay = inDay-yoil;
		for(int i = 0; i < 7;i++) {
			cal.set(inYear, inMonth, inDay+i);  //
			String y = Integer.toString(cal.get(cal.YEAR));
			String m = Integer.toString(cal.get(cal.MONTH)+1);
			String d = Integer.toString(cal.get(cal.DAY_OF_MONTH));
			if(m.length() == 1) m = "0" + m;
			if(d.length() == 1) d = "0" + d;

			arrYMD.add(y + delimiter + m + delimiter + d);
		}
		return arrYMD;
	}

	public static String getCalcDay(String originDay, String dateFormat, int cnt) {
		String laterDay = null;
		try {
			Calendar cal = Calendar.getInstance();
			Date originDate = new Date();
			Date laterDate = new Date();

			if(null == dateFormat) {
				dateFormat = "yyyy-MM-dd";
			}
			SimpleDateFormat format = new SimpleDateFormat(dateFormat); //날짜 형식에 따라서 달리 할 수 있다.
			originDate = format.parse(originDay);

			cal.setTime(laterDate);
			cal.add(Calendar.DATE, cnt);        //laterCnt 만큼 후의 날짜를 구한다. laterCnt 자리에 -7 을 입력하면 일주일전에 날짜를 구할 수 있다.
			laterDate = cal.getTime();
			laterDay = format.format(laterDate);

		} catch (ParseException ex) {
			Trace.log(ex);
		}
		return laterDay;
	}

	public static int getIntervalYear(String oldDate, String formatter, Calendar calendar) {
		SimpleDateFormat format = new SimpleDateFormat(formatter);
		try {
			Date old = format.parse(oldDate);
			Date now = new Date();
			long interval = (now.getTime() - old.getTime()) / 1000 / 60 / 60 / 24 / 365;
			return (int) interval;
		} catch (ParseException e) {
			Trace.log(e);
		}
		return -1;
	}

	public static int getIntervalYear(String oldDate, String formatter) {
		return getIntervalYear(oldDate, formatter, Calendar.getInstance());
	}
	
	
	public static void main(String[] args) {
//		Date d = DateUtil.getDateFromStringFormat("2016-06-15 23:55:05", "yyyy-MM-dd HH:mm:ss");
//		System.out.println(d);
//
//		String d2 = DateUtil.getStringDateFromDate(d, "MM.dd.yyyy");
//		System.out.println(d2);


		/*
		System.out.println(System.currentTimeMillis());
		System.out.println(DateTools.isEquals("13390358887881012201".substring(0, 13), DateTools.YEAR));
		System.out.println(DateTools.isEquals("13390358887881012201".substring(0, 13), DateTools.MONTH));
		System.out.println(DateTools.isEquals("13390358887881012201".substring(0, 13), DateTools.DAY));
		System.out.println(DateTools.isEquals("13390358887881012201".substring(0, 13), DateTools.HOUR));
		System.out.println(DateTools.isEquals("13390358887881012201".substring(0, 13), DateTools.MINUTE));
		System.out.println(DateTools.isEquals("13418883357471011114".substring(0, 13), DateTools.SECOND));

		System.out.println(DateTools.isEquals(System.currentTimeMillis(), DateTools.YEAR));
		System.out.println(DateTools.isEquals(System.currentTimeMillis(), DateTools.MONTH));
		System.out.println(DateTools.isEquals(System.currentTimeMillis(), DateTools.DAY));
		System.out.println(DateTools.isEquals(System.currentTimeMillis(), DateTools.HOUR));
		System.out.println(DateTools.isEquals(System.currentTimeMillis(), DateTools.MINUTE));
		System.out.println(DateTools.isEquals(System.currentTimeMillis(), DateTools.SECOND));

		System.out.println(DateTools.getDateFromUnixTime(System.currentTimeMillis(), DateTools.YEAR));
		System.out.println(DateTools.getDateFromUnixTime(System.currentTimeMillis(), DateTools.MONTH));
		System.out.println(DateTools.getDateFromUnixTime(System.currentTimeMillis(), DateTools.DAY));
		System.out.println(DateTools.getDateFromUnixTime(System.currentTimeMillis(), DateTools.HOUR));
		System.out.println(DateTools.getDateFromUnixTime(System.currentTimeMillis(), DateTools.MINUTE));

		System.out.println(DateUtil.getDateFromUnixTime(Long.parseLong("1341888335747"), DateUtil.SECOND));
		*/



//		System.out.println(DateUtil.getDateUntilDay(Calendar.getInstance(), ""));
//		System.out.println(DateUtil.getDateUntilTimes(Calendar.getInstance(), ""));
//		System.out.println(DateUtil.getIntervalYear("1999-12-07", "yyyy-MM-dd"));
	}
}
