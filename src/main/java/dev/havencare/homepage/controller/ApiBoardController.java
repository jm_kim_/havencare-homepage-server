package dev.havencare.homepage.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import dev.havencare.homepage.controller.handler.*;
import dev.havencare.homepage.controller.request.*;
import dev.havencare.homepage.controller.response.*;
import dev.havencare.homepage.service.CustomUserDetails;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1")
public class ApiBoardController {
  static Logger logger = LoggerFactory.getLogger(ApiBoardController.class);

  private GetBoardListHandler getBoardListHandler;

  @RequestMapping(method = RequestMethod.POST, value = "board/list", consumes = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "게시판 목록 조회")
  GetBoardListResponse getBoardList(@RequestBody GetBoardListRequest req) {
    return getBoardListHandler.execute(null, req);
  }

  private AddBoardHandler addBoardHandler;

  @RequestMapping(method = RequestMethod.POST, value = "board/add", consumes = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "게시판 등록")
  AddBoardResponse addBoard(@RequestBody AddBoardRequest req) {
    return addBoardHandler.execute(null, req);
  }

  private ReviseBoardHandler reviseBoardHandler;

  @RequestMapping(method = RequestMethod.POST, value = "board/revise", consumes = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "게시판 수정")
  ReviseBoardResponse reviseBoard(@RequestBody ReviseBoardRequest req) {
    return reviseBoardHandler.execute(null, req);
  }

  private RemoveBoardHandler removeBoardHandler;

  @RequestMapping(method = RequestMethod.POST, value = "board/remove", consumes = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "게시판 삭제")
  RemoveBoardResponse removeBoard(@RequestBody RemoveBoardRequest req) {
    return removeBoardHandler.execute(null, req);
  }

  private GetBoardHandler getBoardHandler;

  @RequestMapping(method = RequestMethod.POST, value = "board/get", consumes = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "게시판 조회")
  GetBoardResponse getBoard(@RequestBody GetBoardRequest req) {
    return getBoardHandler.execute(null, req);
  }
}
