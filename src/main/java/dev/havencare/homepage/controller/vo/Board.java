package dev.havencare.homepage.controller.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import java.util.HashMap;
import java.util.List;
import java.sql.Date;
import java.sql.Time;

@ApiModel(description = "게시판")
@Data
public class Board {

  public String boardCode = "";

  public String boardType = "";

  public String boardTitle = "";

  public String boardText = "";

  public String viewStartDate = "";

  public String viewEndDate = "";

  public int viewCount = 0;

  public String insertDt = "";

  public String updateDt = "";

  public String deleteDt = "";
}
