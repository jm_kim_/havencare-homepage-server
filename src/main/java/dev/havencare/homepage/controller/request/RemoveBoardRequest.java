package dev.havencare.homepage.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

@ApiModel(description = "게시판 삭제")
@Data
public class RemoveBoardRequest {

  @ApiParam(value = "null", required = true)
  private String boardCode;
}
