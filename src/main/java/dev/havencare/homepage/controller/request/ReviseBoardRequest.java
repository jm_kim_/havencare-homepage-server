package dev.havencare.homepage.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

@ApiModel(description = "게시판 수정")
@Data
public class ReviseBoardRequest {

  @ApiParam(value = "null", required = true)
  private String boardCode;

  @ApiParam(value = "null", required = true)
  private String boardType;

  @ApiParam(value = "null", required = true)
  private String boardTitle;

  @ApiParam(value = "null", required = true)
  private String boardText;

  @ApiParam(value = "null", required = true)
  private String viewStartDate;

  @ApiParam(value = "null", required = true)
  private String viewEndDate;
}
