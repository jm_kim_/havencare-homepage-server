package dev.havencare.homepage.controller.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiParam;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;
import java.util.List;

@ApiModel(description = "게시판 등록")
@Data
public class AddBoardRequest {

  @ApiParam(value = "null", required = true)
  private String boardType;

  @ApiParam(value = "null", required = true)
  private String boardTitle;

  @ApiParam(value = "null", required = true)
  private String boardText;

  @ApiParam(value = "null", required = true)
  private String viewStartDate;

  @ApiParam(value = "null", required = true)
  private String viewEndDate;
}
