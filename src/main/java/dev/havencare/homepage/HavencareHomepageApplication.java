package dev.havencare.homepage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HavencareHomepageApplication {

    public static void main(String[] args) {
        SpringApplication.run(HavencareHomepageApplication.class, args);
    }

}
